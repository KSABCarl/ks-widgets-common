import dataLoader from './storage/data.js';
import { storeState, retrieveState } from './storage/state.js';
import l10n from './text/l10n.js';
import translit from './text/transliterate.js';

import './style.css';

const data = {
  load: dataLoader
};

const state = {
  store: storeState,
  retrieve: retrieveState
};

export {
  l10n,
  translit,
  data,
  state
};
