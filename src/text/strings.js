export default {
  sv: {
    'sv_se': 'Svenska',
    'fr_fr': 'Franska',
    'en_uk': 'English',

    'clickExcercise': 'Klicka här för att öva på glosorna.',
    'interactiveExcercises': 'Interaktiva övningar',
    'wordExcercises': 'Ordövningar',
    'return': 'Återvänd',
    'playAgain': 'Spela igen',
    'startPage': 'Startsida',
    'showList': 'Visa ordlista',
    'playHangman': 'Spela Hänga gubbe',
    'descListing': 'Beskrivning ordlista',
    'descHangman': 'Beskrivning hänga gubbe',

    'correct': 'Rätt!',
    'hangman': 'Hänga gubbe',
    'hangnmanQuestion': 'Översätt glosa',
    'translatesTo': 'översätts till',
    'usedLetters': 'Avända bokstäver',
    'number': 'Nr',
    'of': 'av',
    'completed': 'Du har klarat alla glosor!'
  },

  en: {
    'sv_se': 'Swedish',
    'fr_fr': 'French',
    'en_uk': 'English',

    'clickExcercise': 'Click here to practice the words.',
    'interactiveExcercises': 'Interactive Excercises',
    'wordExcercises': 'Word Excercises',
    'return': 'Return',
    'playAgain': 'Play again',
    'startPage': 'Start page',
    'showList': 'Show word list',
    'playHangman': 'Play Hangman',
    'descListing': 'Description word listing',
    'descHangman': 'Description hangman',

    'correct': 'Correct!',
    'hangman': 'Hangman',
    'hangnmanQuestion': 'Translate',
    'translatesTo': 'translates to',
    'usedLetters': 'Used letters',
    'number': 'No.',
    'of': 'of',
    'completed': 'You have completed all translations!'
  }
};
