import 'whatwg-fetch';
import LocalizedStrings from 'react-localization';

import strings from './strings.js';

export default new LocalizedStrings(strings);
