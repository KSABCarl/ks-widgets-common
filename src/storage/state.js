// function registerStats(user, key, data) {
//   // skicka data till lagring
//   // user = epost
//   // key = nyckel för komponent (Hangman etc)
//   // data är infon som ska lagras – olika beroende på key
//   // data lagras efter datum (sköts backend)
// }

// function getStats(user, key, which='latest') {
//   // hämta data från lagring
//   // user = epost
//   // key = nyckel för komponent (Hangman etc)
//   // which = ts för item, om "latest" så senaste
//   // returnerar object:
//   //   {
//   //      user: user,
//   //      key: key,
//   //      count: nr,
//   //      items: [
//   //        {ts: timestamp, item: {}}
//   //      ],
//   //      combined: {} // kumulativ statistik
//   //    }
// }

function storeState(stateID, state) {
  localStorage.setItem(stateID, JSON.stringify(state));
  // Lägg till fjärrlagring av state
};

function retrieveState(stateID) {
  let data = localStorage.getItem(stateID);

  if (data) {
    return JSON.parse(data);
  }
  return false;
  // Lägg till fjärrhämtning av state
};

export {
  storeState,
  retrieveState
};
