import 'whatwg-fetch';

let datastore = {};

function dataLoader(datasrc) {
  if (datastore[datasrc]) {
    return datastore[datasrc];
  };

  let data = fetch('https://wordlists.talgdank.se/?id=' + datasrc).then(res => {
    const contType = res.headers.get('Content-Type');

    if (contType.indexOf('json') > 0) {
      return res.json();
    };
    return {};
  });

  datastore[datasrc] = data;
  return data;
}

export default dataLoader;
