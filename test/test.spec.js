import TestUtils from 'react-addons-test-utils';
import React from 'react';
import App from '../src/App.js';

var component;
var spy = sinon.spy();

describe('Given an instance of the Component', () => {
  describe('when we render the component', () => {
    before(() => {
      component = TestUtils.renderIntoDocument(<App />);
//      component = TestUtils.renderIntoDocument(<App onRender={ spy } />);
    });
    it('should render a div', () => {
      var div = TestUtils.scryRenderedDOMComponentsWithTag(component, 'div');

      expect(div).to.have.length.above(0, 'Expected to have element with tag <div>');
//      expect(spy).to.be.calledOnce;
    });
  });
});
