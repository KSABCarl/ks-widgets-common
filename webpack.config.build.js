const webpack = require('webpack');
var env = process.env.WEBPACK_ENV || 'dev';
const path = require('path');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebpackDevServer = require('webpack-dev-server');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const appName = 'library';

let plugins = [
  new MiniCssExtractPlugin({
    filename: appName + '.min.css'
  }),
];

let start = {
  mode: 'production',
  context: path.resolve(__dirname, 'src'),
  entry: {
    scripts: 'index.js'
  },
  module: {
    rules: [
    {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader"
        ]
      },
    {
      test: /\.(woff|woff2)$/,
      loader: 'file-loader',
      options: {
        name: './fonts/[name].[ext]'
      }
    },
    {
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['react', 'es2015']
      }
    },
    {
      test: /(\.jsx|\.js)$/,
      loader: 'eslint-loader',
      exclude: /node_modules/
    }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: appName + '.min.js',
    library: 'widgets',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  externals: {
    react: 'react',
    'react-dom': 'react-dom'
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules']
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true
        },
        sourceMap: true
      })
    ]
  },
  plugins: plugins

};

module.exports = start;
